export class Article {

  id: number;
  wording: string;
  author: string;
  content: string;
  urlImage: string;
  color: string;
  category: string;
  sources: string[];
}
