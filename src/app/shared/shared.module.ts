import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [],
  imports: [],
  exports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule, RouterModule]
})
export class SharedModule { }
