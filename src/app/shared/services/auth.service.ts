import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../models/user';

import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn = new BehaviorSubject<boolean>(false); // {1}

  get isLoggedIn() {
    this.loggedIn.next(localStorage.getItem('loginSessId') && localStorage.getItem('loginSessId').length !== 0);
    return this.loggedIn.asObservable(); // {2}
  }

  get id(): string {
    return localStorage.getItem('loginSessId');
  }

  get token(): string {
    return localStorage.getItem('bearerToken');
  }

  set loginSessId(id: number) {
    localStorage.setItem('loginSessId', id.toString());
  }

  set bearerToken(token: string) {
    localStorage.setItem('bearerToken', token);
  }

  set role(role: string) {
    localStorage.setItem('role', role);
  }

  decodeToken(token: string): {id: number} {
    console.log(token)
    return jwt_decode(token);
  }

  constructor(
    private router: Router,
    private http: HttpClient
  ) {}

   login(user: {email: string, password: string}): Observable<{token: string}>{
    console.log(user);
    return this.http.post<{token: string}>(environment.apiUrl + '/auth', user);
  }

  logout() {
    localStorage.clear();
    // this.loggedIn.next(false);
    this.router.navigate(['/']);
  }

  register(user: User): Observable<{id: string, token: string, role: string}> {
    return this.http.post<{id: string, token: string, role: string}>(environment.apiUrl + '/user/register', user);
  }

  static get isAdmin(): boolean {
    return localStorage.getItem('role') === 'admin';
  }

}
