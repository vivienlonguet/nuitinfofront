import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Article} from '../models/article';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {getEntryPointInfo} from '@angular/compiler-cli/ngcc/src/packages/entry_point';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  getAll(): Observable<Article[]> {

    return this.http.get<Article[]>(environment.apiUrl + '/articles', {headers: {
        'Content-Type': 'application/ld+json',
        Accept: 'application/json'}});
  }

  search(categories: string): Observable<Article[]> {

    return this.http.get<Article[]>(environment.apiUrl + '/articles?category=' + categories, {headers: {
        'Content-Type': 'application/ld+json',
        Accept: 'application/json'}});
  }


}


