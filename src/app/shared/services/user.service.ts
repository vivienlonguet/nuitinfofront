import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {User} from '../models/user';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  updateHighscore(id: number, highscore: number): Observable<User> {
    return this.http.put<any>(environment.apiUrl + '/users/' + id, {highScore: highscore}, {
      headers: {
        Authorization: 'Bearer ' + this.authService.token
      }
    });
  }
}
