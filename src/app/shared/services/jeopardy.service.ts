import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class JeopardyService {

  constructor(private httpClient: HttpClient) {
  }

  getRandomQuestion() {
    return this.httpClient.get<Question[]>('http://jservice.io/api/random')
  }
}

export class Category {
  id: number;
  title: string;
  created_at: Date;
  updated_at: Date;
  clues_count: number;
}

export class Question {
  id: number;
  answer: string;
  question: string;
  value: number;
  airdate: Date;
  created_at: Date;
  updated_at: Date;
  category_id: number;
  game_id?: any;
  invalid_count?: any;
  category: Category;
}

