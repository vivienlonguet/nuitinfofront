import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../shared/services/article.service';
import {Article} from '../../shared/models/article';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  articles: Article[];
  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit() {
    this.articleService.getAll().subscribe(articles => {
      this.articles = articles;
    }, error => console.log(error))
  }

  search(keywords: string) {
    console.log(keywords)
    this.articleService.search(keywords).subscribe(res => {
      console.log(res)
      this.articles = res;
    }, error => console.log(error));
  }

  showArticle(article: Article) {
    console.log(article);
    this.router.navigate(['/article'], {state: { article}});
  }

}
