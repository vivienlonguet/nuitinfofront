import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ArticleService} from '../../shared/services/article.service';
import {Article} from '../../shared/models/article';
import {AuthService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';
import {Color} from '../../shared/models/color';

@Component({
  selector: 'app-one-article',
  templateUrl: './one-article.component.html',
  styleUrls: ['./one-article.component.scss']
})
export class OneArticleComponent implements OnInit {

  article: Article;


  constructor(private fb: FormBuilder, private articleService: ArticleService, private authService: AuthService, private router: Router) {
    this.article = this.router.getCurrentNavigation() ? this.router.getCurrentNavigation().extras.state.article : router.navigate(['/']);
  }

  ngOnInit() {
  }

}
