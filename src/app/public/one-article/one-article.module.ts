import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {OneArticleComponent} from './one-article.component';



@NgModule({
  declarations: [OneArticleComponent],
  imports: [
    SharedModule
  ]
})
export class OneArticleModule { }
