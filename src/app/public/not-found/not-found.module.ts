import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {NotFoundComponent} from './not-found.component';
import {MatGridListModule} from "@angular/material/grid-list";


@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    SharedModule,
    MatGridListModule
  ]
})
export class NotFoundModule { }
