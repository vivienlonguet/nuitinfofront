import {Component, OnInit} from '@angular/core';
import {JeopardyService, Question} from "../../shared/services/jeopardy.service";
import {UserService} from "../../shared/services/user.service";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'app-not-found-component',
  templateUrl: './not-found.html',
  styleUrls: ['./not-found.scss']
})
export class NotFoundComponent implements OnInit {

  question: Question;
  userAnswer: String;
  correctAnswer: boolean;
  points = 0;

  highscore = JSON.parse(localStorage.getItem('points'))


  constructor(private jeopardyService: JeopardyService, private authService: AuthService, private userService: UserService) {
  }

  ngOnInit() {
    this.getRandomQuestion();
  }

  getRandomQuestion() {
    this.correctAnswer = false;
    this.userAnswer = "";
    return this.jeopardyService.getRandomQuestion().subscribe(question => {
      if (question[0]) {
        this.question = question[0];
      }
    });
  }

  getRerollCost() {
    return this.question.value * 0.75;
  }

  revealAnswer() {
    this.correctAnswer = true;
    this.managePoints(this.getRerollCost() * -1);
  }

  managePoints(points: number) {
    this.points += points;
    if (points < 0) {
      this.points = 0;
    }
  }

  checkAnswer() {
    this.correctAnswer = this.question.answer.toLowerCase().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
      == this.userAnswer.toLowerCase().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    if (this.correctAnswer) {
      this.managePoints(this.question.value);
    } else {
      this.managePoints((this.question.value) / 5 * -1);
    }
  }

  updateUserHighscore() {
    if (this.points > JSON.parse(localStorage.getItem('points'))) {
      localStorage.setItem('points', JSON.stringify(this.points));
    }
    if (this.authService.isLoggedIn) {
      const id = this.authService.decodeToken(this.authService.token).id;
      this.userService.updateHighscore(id, JSON.parse(localStorage.getItem('points')));
    }

  }
}
